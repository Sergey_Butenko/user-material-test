<?php

namespace Tests\Feature;

use App\Material;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MaterialUpdateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanUpdateMaterial()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $material = factory(Material::class)->create([
            'user_id' => $user->id
        ]);
        $materialData = [
            'title' => 'new test title',
            'content' => 'new test content'
        ];

        $response = $this->actingAs($user, 'api')
            ->json('PUT', "api/users/{$user->id}/materials/{$material->id}", $materialData);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseHas('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotUpdateMaterialOfOtherUsers()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $material = factory(Material::class)->create([
            'user_id' => $secondUser->id
        ]);
        $materialData = [
            'title' => 'new title',
            'content' => 'new content'
        ];

        $response = $this->actingAs($user, 'api')
            ->json('PUT', "api/users/{$secondUser->id}/materials/{$material->id}", $materialData);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJsonMissing([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseMissing('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanUpdateMaterialOfAnyUser()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $secondUser = factory(User::class)->create();
        $material = factory(Material::class)->create([
            'user_id' => $secondUser->id
        ]);
        $materialData = [
            'title' => 'test title',
            'content' => 'test content'
        ];

        $response = $this->actingAs($user, 'api')
            ->json('PUT', "api/users/{$secondUser->id}/materials/{$material->id}", $materialData);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseHas('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }
}
