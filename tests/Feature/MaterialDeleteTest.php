<?php

namespace Tests\Feature;

use App\Material;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MaterialDeleteTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        $this->seed(\RolesTableSeeder::class);
    }

    /**
     * @test
     */
    public function testUserCanDeleteHisMaterial()
    {
        $user = factory(User::class)->create();
        $material = factory(Material::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$user->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('materials', [
            'title' => $material->title,
            'content' => $material->content
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotDeleteOtherUserMaterial()
    {
        $user = $this->createUser();
        $secondUser = $this->createUser();
        $material = factory(Material::class)->create([
            'user_id' => $secondUser->id
        ]);

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$secondUser->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('materials', [
            'title' => $material->title,
            'content' => $material->content
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanDeleteMaterialOfAnyUser()
    {
        $user = $this->createAdmin();
        $secondUser = $this->createUser();
        $material = factory(Material::class)->create([
            'user_id' => $secondUser->id
        ]);

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$secondUser->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('materials', [
            'title' => $material->title,
            'content' => $material->content
        ]);
    }

    private function createUser() {
        return factory(User::class)->create();
    }

    private function createAdmin() {
        $user = $this->createUser()->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));

        return $user;
    }
}
