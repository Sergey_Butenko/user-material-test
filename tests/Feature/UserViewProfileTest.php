<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserViewProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanViewHisProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$user->id}");

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'email' => $user->email,
            'id'    => $user->id
        ]);
        $response->assertSee('photo');
    }

    /**
     * @test
     */
    public function testUserCannotSeeOtherProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$secondUser->id}");

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJsonMissing([
            'email' => $secondUser->email,
            'id' => $secondUser->id
        ]);
        $response->assertDontSee('access_token');
    }

    /**
     * @test
     */
    public function testAdminCanSeeAnyProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $secondUser = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$secondUser->id}");

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'email' => $secondUser->email,
            'id'    => $secondUser->id
        ]);
        $response->assertSee('photo');
    }
}
