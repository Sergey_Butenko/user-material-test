<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanRegister()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->make();
        Storage::fake('avatars');
        $response = $this->json('POST', '/api/auth/register', [
            'email' => $user->email,
            'password' => 'secret',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(201);
        $response->assertJsonFragment([
            'email' => $user->email
        ]);
        $response->assertSee('photo');
        $this->assertDatabaseHas('users', [
            'email' => $user->email
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotUseInvalidEmail()
    {
        $this->seed(\RolesTableSeeder::class);
        Storage::fake('avatars');
        $response = $this->json('POST', '/api/auth/register', [
            'email' => 'invalid@email',
            'password' => 'secret',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $this->assertDatabaseMissing('users', [
            'email' => 'invalid@email'
        ]);
    }

    public function testUsersPasswordMustBeMinSixSymbols()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->make();
        Storage::fake('avatars');
        $response = $this->json('POST', '/api/auth/register', [
            'email' => $user->email,
            'password' => '12345',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $this->assertDatabaseMissing('users', [
            'email' => 'invalid@email'
        ]);
    }
}
