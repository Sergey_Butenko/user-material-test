<?php

namespace Tests\Feature;

use App\Material;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MaterialCreateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanCreateMaterial()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $materialData = [
            'title' => 'test title',
            'content' => 'test content'
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$user->id}/materials", $materialData);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseHas('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotCreateMaterialForOtherUsers()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $material = factory(Material::class)->make([
            'user_id' => $secondUser->id
        ]);
        $materialData = [
            'title' => $material->title,
            'content' => $material->content
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$secondUser->id}/materials", $materialData);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJsonMissing([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseMissing('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanCreateMaterialForAnyUser()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $secondUser = factory(User::class)->create();
        $materialData = [
            'title' => 'test title',
            'content' => 'test content'
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$secondUser->id}/materials", $materialData);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment([
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
        $this->assertDatabaseHas('materials', [
            'title' => $materialData['title'],
            'content' => $materialData['content']
        ]);
    }
}
