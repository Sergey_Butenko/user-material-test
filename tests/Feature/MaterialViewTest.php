<?php

namespace Tests\Feature;

use App\Material;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MaterialViewTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanViewHisMaterial()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $material = factory(Material::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$user->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'id' => $material->id,
            'title' => $material->title,
            'content' => $material->content,
            'created_at' => $material->created_at->format('Y-m-d')
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotViewOtherUserMaterialInfo()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $material = factory(Material::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$user->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJsonMissing([
            'title' => $material->title,
            'content' => $material->content,
            'created_at' => $material->created_at->format('Y-m-d')
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanViewMaterialInfoOfAnyUser()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $material = factory(Material::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('GET', "api/users/{$user->id}/materials/{$material->id}");

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'id' => $material->id,
            'title' => $material->title,
            'content' => $material->content,
            'created_at' => $material->created_at->format('Y-m-d')
        ]);
    }
}
