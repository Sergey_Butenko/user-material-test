<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDeleteProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanDeleteHisProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$user->id}");

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email,
            'id' => $user->id
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotDeleteOtherProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$secondUser->id}");

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('users', [
            'email' => $secondUser->email
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanDeleteAnyProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $secondUser = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->json('DELETE', "api/users/{$secondUser->id}");

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('users', [
            'email' => $secondUser->email
        ]);
    }
}
