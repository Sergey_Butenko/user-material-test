<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanLogin()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'secret',
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'email' => $user->email
        ]);
        $response->assertSee('access_token');
    }

    /**
     * @test
     */
    public function testUserCannotLoginWithWrongCredentials()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'wrongpassword',
        ]);

        $response->assertStatus(401);
        $response->assertDontSee('access_token');
    }
}
