<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserUpdateProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testUserCanUpdateHisProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        Storage::fake('avatars');
        $data = [
            'email' => 'new.email@email.com',
            'password' => 'newpassword111',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$user->id}", $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'email' => $data['email'],
            'id'    => $user->id
        ]);
        $response->assertSee('photo');
        $this->assertDatabaseHas('users', [
            'email' => $data['email']
        ]);
    }

    /**
     * @test
     */
    public function testUserCannotUpdateOtherProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        Storage::fake('avatars');
        $data = [
            'email' => 'new.email@email.com',
            'password' => 'newpassword111',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$secondUser->id}", $data);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJsonMissing([
            'email' => $data['email'],
            'id'    => $user->id
        ]);
        $response->assertDontSee('photo');
        $this->assertDatabaseHas('users', [
            'email' => $secondUser->email
        ]);
    }

    /**
     * @test
     */
    public function testAdminCanUpdateAnyProfile()
    {
        $this->seed(\RolesTableSeeder::class);
        $user = factory(User::class)->create();
        $user->attachRole(Role::getRoleIdByName(Role::ADMIN_ROLE));
        $secondUser = factory(User::class)->create();
        Storage::fake('avatars');
        $data = [
            'email' => 'new.email@email.com',
            'password' => 'newpassword111',
            'photo' => UploadedFile::fake()->image('avatar.jpg')
        ];

        $response = $this->actingAs($user, 'api')
            ->json('POST', "api/users/{$secondUser->id}", $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([
            'email' => $data['email'],
        ]);
        $response->assertSee('photo');
        $this->assertDatabaseHas('users', [
            'email' => $data['email']
        ]);
    }
}
