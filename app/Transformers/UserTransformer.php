<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['materials'];

    /**
     * @var array
     */
    protected $defaultIncludes = ['photo'];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        $userInfo = [
            'id' => $user->id,
            'email' => $user->email,

        ];
        if($user->access_token) {
            $userInfo['access_token'] = $user->access_token;
        }

        return $userInfo;
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includePhoto(User $user)
    {
        return $this->item($user->photo, new MediaTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeMaterials(User $user)
    {
        return $this->collection($user->materials, new MaterialTransformer());
    }
}
