<?php

namespace App\Transformers;

use App\Media;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    /**
     * @param Media $media
     * @return array
     */
    public function transform(Media $media)
    {
        return [
            'id' => $media->id,
            'url' => $media->url
        ];
    }
}
