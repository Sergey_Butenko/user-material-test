<?php

namespace App\Transformers;

use App\Material;
use League\Fractal\TransformerAbstract;

class MaterialTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = ['user'];

    /**
     * @param Material $material
     * @return array
     */
    public function transform(Material $material)
    {
        return [
            'id' => $material->id,
            'title' => $material->title,
            'content' => $material->content,
            'created_at' => $material->created_at->format('Y-m-d')
        ];
    }

    /**
     * @param Material $material
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Material $material)
    {
        return $this->item($material->user, new UserTransformer());
    }
}
