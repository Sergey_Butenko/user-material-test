<?php

namespace App\Http\Controllers\API\Auth;

use App\Contracts\AuthServiceContract;
use App\Http\Requests\API\Auth\RegisterRequest;
use App\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @param AuthServiceContract $authService
     * @return JsonResponse
     */
    public function register(RegisterRequest $request, AuthServiceContract $authService): JsonResponse
    {
        $user = $authService->register($request->email, $request->password, $request->photo);

        return fractal($user, new UserTransformer())->respond(201);
    }
}
