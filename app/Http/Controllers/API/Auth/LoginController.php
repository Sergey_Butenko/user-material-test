<?php

namespace App\Http\Controllers\API\Auth;

use App\Contracts\AuthServiceContract;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Spatie\Fractal\Fractal;

class LoginController extends Controller
{
    /**
     * @var AuthServiceContract
     */
    private $authService;

    /**
     * LoginController constructor.
     * @param AuthServiceContract $authService
     */
    public function __construct(AuthServiceContract $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Login user in.
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $user = $this->authService->login($request->email, $request->password);

        return fractal($user, new UserTransformer())->respond();
    }

    /**
     * Log user out.
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $this->authService->logout();

        return response()->json(['message' => 'Success.']);
    }

    /**
     * Refresh token.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        $token = $this->authService->refresh();

        return response()->json(['access_token' => $token]);
    }
}
