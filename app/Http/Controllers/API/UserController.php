<?php

namespace App\Http\Controllers\API;

use App\Contracts\UserServiceContract;
use App\Http\Requests\API\User\DeleteUserRequest;
use App\Http\Requests\API\User\GetUserRequest;
use App\Http\Requests\API\User\UpdateUserRequest;
use App\Transformers\UserTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Fractal\Fractal;

class UserController extends Controller
{
    /**
     * @var UserServiceContract
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserServiceContract $userService
     */
    public function __construct(UserServiceContract $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Paginate users. Available only for admin.
     *
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $users = $this->userService->paginate();

        return fractal($users, new UserTransformer())->respond();
    }

    /**
     * Get user profile by id.
     *
     * @param GetUserRequest $request
     * @return JsonResponse
     */
    public function find(GetUserRequest $request): JsonResponse
    {
        $user = $this->userService->find($request->id);

        return fractal($user, new UserTransformer())->respond();
    }

    /**
     * Update user profile.
     *
     * @param UpdateUserRequest $request
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request): JsonResponse
    {
        $user = $this->userService->update($request->email, $request->password, $request->photo, $request->id);

        return fractal($user, new UserTransformer())->respond();
    }

    /**
     * @param DeleteUserRequest $request
     * @return JsonResponse
     */
    public function delete(DeleteUserRequest $request): JsonResponse
    {
        $this->userService->delete($request->id);

        return response()->json(['message' => 'Profile deleted.']);
    }
}
