<?php

namespace App\Http\Controllers\API;

use App\Contracts\MaterialServiceContract;
use App\Http\Requests\API\Material\CreateMaterialRequest;
use App\Http\Requests\API\Material\DeleteMaterialRequest;
use App\Http\Requests\API\Material\GetMaterialRequest;
use App\Http\Requests\API\Material\GetUserMaterialsRequest;
use App\Http\Requests\API\Material\UpdateMaterialRequest;
use App\Transformers\MaterialTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Fractal\Fractal;

class MaterialController extends Controller
{
    /**
     * @var MaterialServiceContract
     */
    private $materialService;

    /**
     * MaterialController constructor.
     * @param MaterialServiceContract $materialService
     */
    public function __construct(MaterialServiceContract $materialService)
    {
        $this->materialService = $materialService;
    }

    /**
     * Get materials of user by user_id
     *
     * @param GetUserMaterialsRequest $request
     * @return JsonResponse
     */
    public function getUserMaterial(GetUserMaterialsRequest $request): JsonResponse
    {
        $materials = $this->materialService->getUserMaterials($request->user_id);

        return fractal($materials, new MaterialTransformer())->respond();
    }

    /**
     * Get all materials. Only for admin.
     *
     * @return JsonResponse
     */
    public function getAllMaterials(): JsonResponse
    {
        $materials = $this->materialService->getAllMaterials();

        return fractal($materials, new MaterialTransformer())->respond();
    }

    /**
     * Get material by material id.
     *
     * @param GetMaterialRequest $request
     * @return JsonResponse
     */
    public function find(GetMaterialRequest $request): JsonResponse
    {
        $material = $this->materialService->find($request->id);

        return fractal($material, new MaterialTransformer())->respond();
    }

    /**
     * Create new Material.
     *
     * @param CreateMaterialRequest $request
     * @return JsonResponse
     */
    public function create(CreateMaterialRequest $request): JsonResponse
    {
        $material = $this->materialService->create($request->title, $request->content, $request->user_id);

        return fractal($material, new MaterialTransformer())->respond(201);
    }

    /**
     * Update Material info by id.
     *
     * @param UpdateMaterialRequest $request
     * @return JsonResponse
     */
    public function update(UpdateMaterialRequest $request): JsonResponse
    {
        $material = $this->materialService->update($request->title, $request->content, $request->id, $request->user_id);

        return fractal($material, new MaterialTransformer())->respond(200);
    }

    /**
     * Delete material.
     *
     * @param DeleteMaterialRequest $request
     * @return JsonResponse
     */
    public function delete(DeleteMaterialRequest $request): JsonResponse
    {
        $this->materialService->delete($request->id);

        return response()->json(['message' => 'Deleted']);
    }
}
