<?php

namespace App\Http\Requests\API\User;

use App\User;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\API\User
 *
 * @property string $email
 * @property string $password
 * @property integer $id
 * @property UploadedFile $photo
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        $data['id'] = $this->route('id');

        return $data;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Gate $gate)
    {
        return $gate->getPolicyFor(User::class)->update($this->user(), $this->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:users,id',
            'email' => [
                'required',
                'email',
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->id)
            ],
            'password' => 'required|string|min:6|max:255',
            'photo' => 'required|image'
        ];
    }
}
