<?php

namespace App\Http\Requests\API\Material;

use App\Material;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateMaterialRequest
 * @package App\Http\Requests\API\Material
 *
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $content
 */
class UpdateMaterialRequest extends FormRequest
{
    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        $data['user_id'] = $this->route('user_id');
        $data['id'] = $this->route('id');

        return $data;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Gate $gate
     * @return bool
     */
    public function authorize(Gate $gate)
    {
        $material = Material::find($this->id);

        return $gate->getPolicyFor(Material::class)->update($this->user(), $material);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'      => 'required|integer|exists:materials,id',
            'user_id' => 'required|integer|exists:users,id',
            'title'   => 'required|string|max:255',
            'content' => 'required|string:max:255'
        ];
    }
}
