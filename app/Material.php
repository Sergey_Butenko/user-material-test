<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Material
 * @package App
 *
 *
 * @property integer $user_id
 * @property string $title
 * @property string $content
 * @property User $user
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Material extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'content'
    ];

    protected $casts = [
        'user_id'    => 'integer',
        'title'      => 'string',
        'content'    => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
