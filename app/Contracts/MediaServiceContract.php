<?php


namespace App\Contracts;


use App\Media;
use Illuminate\Http\UploadedFile;

interface MediaServiceContract
{
    /**
     * @param UploadedFile $file
     * @return Media
     */
    public function uploadFile(UploadedFile $file): Media;
}