<?php


namespace App\Contracts;


use App\Material;
use Illuminate\Pagination\LengthAwarePaginator;

interface MaterialServiceContract
{
    /**
     * @param int $userId
     * @return LengthAwarePaginator
     */
    public function getUserMaterials(int $userId): LengthAwarePaginator;

    /**
     * @return LengthAwarePaginator
     */
    public function getAllMaterials(): LengthAwarePaginator;

    /**
     * @param int $id
     * @return Material
     */
    public function find(int $id): Material;

    /**
     * @param string $title
     * @param string $content
     * @param int $userId
     * @return Material
     */
    public function create(string $title, string $content, int $userId): Material;

    /**
     * @param string $title
     * @param string $content
     * @param int $id
     * @param int $userId
     * @return Material
     */
    public function update(string $title, string $content, int $id, int $userId): Material;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}