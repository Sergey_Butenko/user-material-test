<?php


namespace App\Contracts;


use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;

interface UserServiceContract
{
    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator;

    /**
     * @param int $id
     * @return User
     */
    public function find(int $id): User;

    /**
     * @param string $email
     * @param string $password
     * @param UploadedFile $photo
     * @param int $id
     * @return User
     */
    public function update(string $email, string $password, UploadedFile $photo, int $id): User;

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id);
}