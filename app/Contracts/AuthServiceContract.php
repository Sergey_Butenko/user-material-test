<?php


namespace App\Contracts;


use App\User;
use Illuminate\Http\UploadedFile;

interface AuthServiceContract
{
    /**
     * @param string $email
     * @param string $password
     * @return mixed
     */
    public function login(string $email, string $password): User;

    /**
     * @return mixed
     */
    public function logout();

    /**
     * @return mixed
     */
    public function refresh(): string;

    /**
     * @param string $email
     * @param string $password
     * @param UploadedFile $photo
     * @return User
     */
    public function register(string $email, string $password, UploadedFile $photo): User;
}