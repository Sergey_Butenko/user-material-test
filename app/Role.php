<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const ADMIN_ROLE = 'admin';

    const USER_ROLE = 'user';

    /**
     * @param string $name
     * @return mixed
     */
    public static function getRoleIdByName(string $name)
    {
        return self::where('name', $name)->first()->id;
    }
}
