<?php

namespace App\Providers;

use App\Contracts\AuthServiceContract;
use App\Contracts\MaterialServiceContract;
use App\Contracts\MediaServiceContract;
use App\Contracts\UserServiceContract;
use App\Repositories\MaterialRepository;
use App\Repositories\MaterialRepositoryEloquent;
use App\Repositories\MediaRepository;
use App\Repositories\MediaRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;
use App\Services\AuthService;
use App\Services\MaterialService;
use App\Services\MediaService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(AuthServiceContract::class, AuthService::class);
        app()->bind(UserRepository::class, UserRepositoryEloquent::class);
        app()->bind(MediaRepository::class, MediaRepositoryEloquent::class);
        app()->bind(MediaServiceContract::class, MediaService::class);
        app()->bind(UserServiceContract::class, UserService::class);
        app()->bind(MaterialServiceContract::class, MaterialService::class);
        app()->bind(MaterialRepository::class, MaterialRepositoryEloquent::class);
    }
}
