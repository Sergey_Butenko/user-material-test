<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereUserIdCriteria.
 *
 * @package namespace App\Criteria;
 */
class WhereUserIdCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private $userId;

    /**
     * WhereUserIdCriteria constructor.
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('user_id', $this->userId);
    }
}
