<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class Media
 * @package App
 *
 *
 * @property integer $id
 * @property string $filename
 * @property string $mime_type
 * @property integer $size
 * @property string $url
 */
class Media extends Model
{
    /**
     *
     */
    public static $avatarsFolder = 'avatars';

    /**
     * @var array
     */
    protected $fillable = [
        'filename',
        'mime_type',
        'size',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'filename' => 'string',
        'mime_type' => 'string',
        'size' => 'integer'
    ];

    /**
     * @return mixed
     */
    public function getUrlAttribute()
    {
        $path = self::$avatarsFolder . '/'. $this->filename;

        return Storage::temporaryUrl($path, now()->addMinutes(10));
    }
}
