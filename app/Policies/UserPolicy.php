<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  integer $id
     * @return mixed
     */
    public function view(User $user, int $id)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $id ? true : false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  integer $id
     * @return mixed
     */
    public function update(User $user, int $id)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $id ? true : false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  integer $id
     * @return mixed
     */
    public function delete(User $user, int $id)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $id ? true : false;
    }
}
