<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Material;
use Illuminate\Auth\Access\HandlesAuthorization;

class MaterialPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $userId
     * @return bool
     */
    public function getUserMaterials(User $user, $userId)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $userId ? true : false;
    }

    /**
     * @param User $user
     * @param Material $material
     * @return bool
     */
    public function view(User $user, Material $material)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $material->user_id ? true : false;
    }

    /**
     * @param User $user
     * @param $userId
     * @return bool
     */
    public function create(User $user, $userId)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $userId ? true : false;
    }

    /**
     * @param User $user
     * @param Material $material
     * @return bool
     */
    public function update(User $user, Material $material)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $material->user_id ? true : false;
    }

    /**
     * @param User $user
     * @param Material $material
     * @return bool
     */
    public function delete(User $user, Material $material)
    {
        return $user->hasRole(Role::ADMIN_ROLE) || $user->id == $material->user_id ? true : false;
    }
}
