<?php


namespace App\Services;


use App\Contracts\MaterialServiceContract;
use App\Criteria\WhereUserIdCriteria;
use App\Material;
use App\Repositories\MaterialRepository;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;

class MaterialService implements MaterialServiceContract
{
    /**
     * @var MaterialRepository
     */
    private $materialRepository;

    /**
     * MaterialService constructor.
     * @param MaterialRepository $materialRepository
     */
    public function __construct(MaterialRepository $materialRepository)
    {
        $this->materialRepository = $materialRepository;
    }

    /**
     * @param int $userId
     * @return LengthAwarePaginator
     */
    public function getUserMaterials(int $userId): LengthAwarePaginator
    {
        $this->materialRepository->pushCriteria(new WhereUserIdCriteria($userId));

        return $this->materialRepository->paginate();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAllMaterials(): LengthAwarePaginator
    {
        return $this->materialRepository->paginate();
    }

    /**
     * @param int $id
     * @return Material
     */
    public function find(int $id): Material
    {
        return $this->materialRepository->find($id);
    }

    /**
     * Create new material.
     *
     * @param string $title
     * @param string $content
     * @param int $userId
     * @return Material
     */
    public function create(string $title, string $content, int $userId): Material
    {
        try {
            $material = $this->materialRepository->create([
                'title'   => $title,
                'content' => $content,
                'user_id' => $userId
            ]);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to create new material.');
        }

        return $material;
    }

    /**
     * Update material.
     *
     * @param string $title
     * @param string $content
     * @param int $id
     * @param int $userId
     * @return Material
     */
    public function update(string $title, string $content, int $id, int $userId): Material
    {
        try {
            $material = $this->materialRepository->update([
                'title'   => $title,
                'content' => $content
            ], $id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to update material info.');
        }

        return $material;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        try {
            $isDeleted = $this->materialRepository->delete($id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to delete material.');
        }

        return $isDeleted;
    }
}