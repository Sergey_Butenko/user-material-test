<?php


namespace App\Services;


use App\Contracts\MediaServiceContract;
use App\Media;
use App\Repositories\MediaRepository;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;

class MediaService implements MediaServiceContract
{
    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * MediaService constructor.
     * @param MediaRepository $mediaRepository
     */
    public function __construct(MediaRepository $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * @param UploadedFile $file
     * @return Media
     */
    public function uploadFile(UploadedFile $file): Media
    {
        try {
            $filename = $this->handleFile($file);

            $media = $this->mediaRepository->create([
                'filename' => $filename,
                'mime_type' => $file->getClientMimeType(),
                'size' => $file->getSize()
            ]);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to upload file.');
        }

        return $media;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function handleFile(UploadedFile $file): string
    {
        $filename = uniqid() . '.' . $file->getClientOriginalExtension();
        $file->storeAs(Media::$avatarsFolder, $filename);

        return $filename;
    }
}