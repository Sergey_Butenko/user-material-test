<?php


namespace App\Services;


use App\Contracts\MediaServiceContract;
use App\Contracts\UserServiceContract;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserService implements UserServiceContract
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MediaServiceContract
     */
    private $mediaService;

    /**
     * AuthService constructor.
     * @param UserRepository $userRepository
     * @param MediaServiceContract $mediaService
     */
    public function __construct(UserRepository $userRepository, MediaServiceContract $mediaService)
    {
        $this->userRepository = $userRepository;
        $this->mediaService = $mediaService;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {
        return $this->userRepository->paginate();
    }

    /**
     * @param int $id
     * @return User
     */
    public function find(int $id): User
    {
        try {
            $user = $this->userRepository->find($id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_NOT_FOUND, 'User not found.');
        }

        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @param UploadedFile $photo
     * @param int $id
     * @return User
     */
    public function update(string $email, string $password, UploadedFile $photo, int $id): User
    {
        DB::beginTransaction();
        try {
            $media = $this->mediaService->uploadFile($photo);
            $user = $this->userRepository->update([
                'email'    => $email,
                'password' => $password,
                'media_id' => $media->id
            ], $id);
        } catch (\Exception $exception) {
            DB::rollback();
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to update profile.');
        }
        DB::commit();

        return $user;
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        try {
            $user = $this->userRepository->delete($id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to delete profile.');
        }

        return $user;
    }
}