<?php


namespace App\Services;


use App\Contracts\AuthServiceContract;
use App\Contracts\MediaServiceContract;
use App\Repositories\UserRepository;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class AuthService implements AuthServiceContract
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MediaServiceContract
     */
    private $mediaService;

    /**
     * AuthService constructor.
     * @param UserRepository $userRepository
     * @param MediaServiceContract $mediaService
     */
    public function __construct(UserRepository $userRepository, MediaServiceContract $mediaService)
    {
        $this->userRepository = $userRepository;
        $this->mediaService = $mediaService;
    }

    /**
     * @param string $email
     * @param string $password
     * @return User
     */
    public function login(string $email, string $password): User
    {
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (! $token = auth()->guard('api')->attempt($credentials)) {
            abort(401, 'Credentials incorrect.');
        }
        $user = auth()->guard('api')->setToken($token)->user();
        $user->setAccessToken($token);

        return $user;
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return mixed
     */
    public function logout()
    {
        return auth()->guard('api')->logout();
    }

    /**
     * Refresh a token.
     *
     * @return string
     */
    public function refresh(): string
    {
        return auth()->guard('api')->refresh();
    }

    /**
     * @param string $email
     * @param string $password
     * @param UploadedFile $photo
     * @return User
     */
    public function register(string $email, string $password, UploadedFile $photo): User
    {
        DB::beginTransaction();
        try {
            $media = $this->mediaService->uploadFile($photo);
            $user = $this->userRepository->create([
                'email' => $email,
                'password' => $password,
                'media_id' => $media->id
            ]);
            $user->attachRole(Role::getRoleIdByName(Role::USER_ROLE));
        } catch (\Exception $exception) {
            DB::rollback();
            //  TODO: clean the media
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to register');
        }
        DB::commit();

        return $user;
    }
}