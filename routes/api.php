<?php

Route::group(['namespace' => 'Auth', 'prefix' => 'auth', 'middleware' => 'auth:api'], function () {
    Route::post('logout', 'LoginController@logout');
    Route::post('refresh', 'LoginController@refresh');


});
Route::group(['middleware' => 'auth:api'], function () {
    // User
    Route::get('users/{id}', 'UserController@find');
    Route::post('users/{id}', 'UserController@update');
    Route::delete('users/{id}', 'UserController@delete');

    // Materials
    Route::get('users/{user_id}/materials', 'MaterialController@getUserMaterial');
    Route::get('users/{user_id}/materials/{id}', 'MaterialController@find');
    Route::post('users/{user_id}/materials', 'MaterialController@create');
    Route::put('users/{user_id}/materials/{id}', 'MaterialController@update');
    Route::delete('users/{user_id}/materials/{id}', 'MaterialController@delete');
});


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => ['auth:api', 'role:admin']], function() {
    Route::get('users', 'UserController@paginate');
    Route::get('materials', 'MaterialController@getAllMaterials');
});

Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'LoginController@login');
});

