<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();
        foreach ($users as $user) {
            if ($user->email === 'admin@admin.com') {
                $user->attachRole(\App\Role::getRoleIdByName(\App\Role::ADMIN_ROLE));
            } else {
                $user->attachRole(\App\Role::getRoleIdByName(\App\Role::USER_ROLE));
            }
        }
    }
}
