<?php

use Faker\Generator as Faker;

$factory->define(\App\Material::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'title' => $faker->company,
        'content' => $faker->text()
    ];
});
