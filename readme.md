## Project install

Установка проекта:
1. git clone git@bitbucket.org:Sergey_Butenko/user-material-test.git
2. cp .env.example .env
3. создать и вставить настройки базы данных в .env файл
4. Создать вторую базу данных (для тестов) и вставить в .env.testing
5. composer install
6. php artisan migrate --seed - создаст таблицы и заполнит их тестовыми данными.
7. Готово

## Project overview

Описание проекта:
1. По максимуму использовал SOLID, все зависит от Абстракций (Interface). Все интерфейсы "байндятся" в AppServiceProvider к конкретной реализации.
2. Код покрыт тестами с использованием PHPUnit
3. Для аутентификации используется JWT token
4. Для стабильности моделей респонсов используется Fractal Transformer.
5. Рауты для админа защищены на уровне middleware
6. Проверка прав на доступ к определенному route происходит в Form Request и Policy
7. Валидация вынесена в Form Request.
8. Все обернуто в try/catch, для нескольких запросов используются транзакции.
9. Для хранения данных используется AWS S3
